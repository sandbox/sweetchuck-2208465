<?php

/**
 * @file
 * StyleGuide integration of styleguide jQuery UI module.
 */

/**
 * Implements hook_styleguide().
 */
function styleguide_jquery_ui_styleguide() {
  $items = array();

  $group = t('jQuery UI');

  $items['jquery_ui_accordion'] = array(
    'group' => $group,
    'title' => t('Accordion'),
    'content' => styleguide_jquery_ui_demo_accordion()
  );

  $items['jquery_ui_autocomplete'] = array(
    'group' => $group,
    'title' => t('Autocomplete'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_autocomplete'),
  );

  $items['jquery_ui_button'] = array(
    'group' => $group,
    'title' => t('Button'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_button'),
  );

  $items['jquery_ui_datepicker'] = array(
    'group' => $group,
    'title' => t('Datepicker'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_datepicker'),
  );

  $items['jquery_ui_dialog'] = array(
    'group' => $group,
    'title' => t('Dialog'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_dialog'),
  );

  $items['jquery_ui_menu'] = array(
    'group' => $group,
    'title' => t('Menu'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_menu'),
  );

  $items['jquery_ui_progressbar'] = array(
    'group' => $group,
    'title' => t('Progressbar'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_progressbar'),
  );

  $items['jquery_ui_slider'] = array(
    'group' => $group,
    'title' => t('Slider'),
    'content' => drupal_get_form('styleguide_jquery_ui_demo_slider'),
  );

  $items['jquery_ui_tabs'] = array(
    'group' => $group,
    'title' => t('Tabs'),
    'content' => styleguide_jquery_ui_demo_tabs()
  );

  $items['jquery_ui_icon'] = array(
    'group' => $group,
    'title' => t('Icon'),
    'content' => drupal_get_form('_styleguide_jquery_ui_demo_icon'),
  );

  return $items;
}

/**
 * Provide accordion widget for the Style guide page.
 *
 * @return array
 *   Render array.
 */
function styleguide_jquery_ui_demo_accordion() {
  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  return array(
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.accordion'),
      ),
    ),
    '#markup' => '<div class="styleguide-jquery-ui-accordion">' .
      '<h4><a href="#">' . styleguide_word(4) . '</a></h4>' .
      '<div>' . styleguide_paragraph(1) . '</div>' .
      '<h4><a href="#">' . styleguide_word(4) . '</a></h4>' .
      '<div>' . styleguide_paragraph(1) . '</div>' .
      '<h4><a href="#">' . styleguide_word(4) . '</a></h4>' .
      '<div>' . styleguide_paragraph(1) . '</div>' .
      '</div>',
  );
}

/**
 * Provide autocomplete widgets for the Style guide page.
 *
 * FormAPI form builder callback.
 */
function styleguide_jquery_ui_demo_autocomplete($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $form['#attributes']['class'][] = 'styleguide-jquery-ui-autocomplete-wrapper';

  $item = menu_get_item('styleguide-jquery-ui/autocomplete/icon');
  $form['drupal'] = array(
    '#type' => 'textfield',
    '#title' => t('Autocomplete (Drupal)'),
    '#autocomplete_path' => 'styleguide-jquery-ui/autocomplete/icon',
    '#access' => $item['access'],
    '#description' => t('jQuery UI icons'),
  );

  $form['native'] = array(
    '#type' => 'textfield',
    '#title' => t('Autocomplete (Native)'),
    '#description' => t('jQuery UI icons'),
    '#attributes' => array(
      'class' => array('styleguide-jquery-ui-autocomplete'),
    ),
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.autocomplete'),
      ),
    ),
  );

  return $form;
}

/**
 * Provide autocomplete widgets for the Style guide page.
 *
 * FormAPI form builder callback. I think the FormAPI is not capable to create
 * <BUTTON> tag.
 */
function styleguide_jquery_ui_demo_button($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $form['#attributes']['class'][] = 'styleguide-jquery-ui-button-wrapper';
  $form['#attached']['js'][] = "$path/js/styleguide_jquery_ui.js";
  $form['#attached']['library'][] = array('system', 'ui.button');

  $form['buttons'] = array(
    '#prefix' => '<div class="styleguide-jquery-ui-buttons">',
    '#suffix' => '</div>',
  );

  $variants = array(
    'p' => t('Button with icon'),
    'pt' => t('Button with right icon and text'),
    'ts' => t('Button with left icon and text'),
    'pts' => t('Button with icons and text'),
    'ps' => t('Button with icons'),
    's' => t('Button with icon'),
    't' => t('Button with text'),
  );
  foreach ($variants as $key => $label) {
    $form['buttons']["icon_$key"] = array(
      '#type' => 'markup',
      '#markup' => sprintf('<button class="styleguide-jquery-ui-button-%s styleguide-jquery-ui-fake-button">%s</button>', $key, $label),
    );
  }

  $form['feedback'] = array(
    '#markup' => '<div class="styleguide-jquery-ui-feedback"></div>',
  );

  return $form;
}

/**
 * Provide date picker widgets for the Style guide page.
 *
 * FormAPI form builder callback.
 */
function styleguide_jquery_ui_demo_datepicker($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $form['controls'] = array(
    '#type' => 'container',
    '#tree' => FALSE,
    '#attributes' => array(
      'class' => array(
        'controls',
      ),
    ),
  );

  $form['controls']['properties'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Properties'),
    '#default_value' => array(),
    '#options' => array(
      'showOtherMonths' => t('Show other months'),
      'selectOtherMonths' => t('Select other months'),
      'showButtonPanel' => t('Show button panel'),
      'changeMonth' => t('Change month'),
      'changeYear' => t('Change year'),
      'showWeek' => t('Week of the year'),
      'restrictDateRange' => t('Restrict date range'),
      'showOn' => t('Icon trigger'),
      'buttonImageOnly' => t('Button image only'),
    ),
    '#attributes' => array(
      'class' => array('styleguide-jquery-ui-buttonset'),
    ),
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.button'),
      ),
    ),
  );
  _styleguide_jquery_ui_attach_recommendations($form['controls']['properties']);

  $form['controls']['widget'] = array(
    '#type' => 'radios',
    '#title' => t('Widget'),
    '#title_display' => 'none',
    '#default_value' => 'inline',
    '#options' => array(
      'inline' => t('Inline'),
      'input' => t('Input'),
      'all' => t('All'),
    ),
    '#attributes' => array(
      'class' => array('styleguide-jquery-ui-buttonset'),
    ),
  );

  $form['controls']['numberOfMonthsRows'] = array(
    '#type' => 'select',
    '#title' => t('Number of month rows'),
    '#default_value' => 1,
    '#options' => drupal_map_assoc(range(1, 12)),
  );

  $form['controls']['numberOfMonthsColumns'] = array(
    '#type' => 'select',
    '#title' => t('Number of month columns'),
    '#default_value' => 1,
    '#options' => drupal_map_assoc(range(1, 12)),
  );

  $form['content']['normal'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
      'class' => array(
        'styleguide-jquery-ui-datepicker',
      ),
    ),
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.datepicker'),
      ),
    ),
    '#states' => array(
      'invisible' => array(
        ':input[name="widget"]' => array('value' => 'inline'),
      ),
    ),
  );

  $form['content']['inline'] = array(
    '#type' => 'container',
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.datepicker'),
      ),
    ),
    '#states' => array(
      'invisible' => array(
        ':input[name="widget"]' => array('value' => 'input'),
      ),
    ),
    'content' => array(
      '#markup' => '<div class="styleguide-jquery-ui-datepicker"></div>',
    ),
  );

  return $form;
}

/**
 * Provide a dialog window widget for the Style guide page.
 *
 * FormAPI form builder callback.
 */
function styleguide_jquery_ui_demo_dialog($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $id = drupal_html_id('styleguide-jquery-ui-dialog-window');
  $form['#attributes']['class'][] = 'styleguide-jquery-ui-dialog-wrapper';
  $form['#attributes']['data-dialog-id'] = $id;

  $form['#attached']['css'][] = "$path/css/styleguide_jquery_ui.css";
  $form['#attached']['js'][] = "$path/js/styleguide_jquery_ui.js";
  $form['#attached']['library'][] = array('system', 'ui.dialog');
  $form['#attached']['library'][] = array('system', 'ui.button');
  _styleguide_jquery_ui_attach_recommendations($form);

  $form['properties'] = array(
    '#type' => 'checkboxes',
    '#required' => FALSE,
    '#title' => t('Properties'),
    '#default_value' => array(),
    '#options' => array(
      'no_close' => t('No close'),
      'no_title' => t('No title'),
      'buttons' => t('Buttons'),
      'modal' => t('Modal'),
    ),
    '#attributes' => array(
      'class' => array('styleguide-jquery-ui-buttonset'),
    ),
  );

  $attributes = array(
    'href' => '#',
    'class' => array(
      'styleguide-jquery-ui-dialog-opener',
      'ui-state-default',
      'ui-corner-all',
    ),
  );
  $form['opener'] = array(
    '#markup' => sprintf(
      '<p><a%s><span class="ui-icon ui-icon-newwin"></span>%s</a></p>',
      drupal_attributes($attributes),
      t('Open dialog')
    ),
  );

  $attributes = array(
    'id' => $id,
    'class' => array('styleguide-jquery-ui-dialog-window'),
    'title' => t('Dialog title'),
    'style' => 'display: none;',
  );
  $form['dialog'] = array(
    '#markup' => sprintf(
      '<div%s>%s</div>',
      drupal_attributes($attributes),
      styleguide_paragraph(1)
    ),
  );

  return $form;
}

/**
 * Provide a menu widget for the Style guide page.
 *
 * FormAPI form builder callback.
 */
function styleguide_jquery_ui_demo_menu($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');
  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $icon_folder = '<span class="ui-icon ui-icon-folder-collapsed"></span>';
  $icon_script = '<span class="ui-icon ui-icon-script"></span>';
  $icon_menu = '<span class="ui-icon ui-menu-icon ui-icon-triangle-1-e"></span>';

  $form['#attributes']['class'][] = 'clearfix';

  $form['content'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="styleguide-jquery-ui-menu-wrapper clearfix">',
    '#markup' => '',
    '#suffix' => '</div>',
    '#attached' => array(
      'css' => array("$path/css/styleguide_jquery_ui.css"),
      'js' => array("$path/js/styleguide_jquery_ui.js"),
      'library' => array(
        array('system', 'ui.autocomplete'),
        array('system', 'ui.button'),
      ),
    ),
  );
  _styleguide_jquery_ui_attach_recommendations($form['content']);

  $form['content']['#markup'] = <<< HTML
<ul class="styleguide-jquery-ui-menu">
  <li><a href="#">{$icon_folder}{$icon_menu}includes</a>
    <ul>
      <li><a href="#">{$icon_script}actions.inc</a></li>
      <li><a href="#">{$icon_script}ajax.inc</a></li>
      <li><a href="#">{$icon_script}archiver.inc</a></li>
    </ul>
  </li>
  <li><a href="#">{$icon_folder}misc</a></li>
  <li><a href="#">{$icon_folder}modules</a></li>
  <li><a href="#">{$icon_folder}profiles</a></li>
  <li><a href="#">{$icon_folder}scripts</a></li>
  <li><a href="#">{$icon_folder}sites</a></li>
  <li><a href="#">{$icon_folder}{$icon_menu}themes</a>
    <ul>
      <li><a href="#">{$icon_folder}bartik</a></li>
      <li><a href="#">{$icon_folder}engines</a></li>
      <li><a href="#">{$icon_folder}garland</a></li>
      <li><a href="#">{$icon_folder}seven</a></li>
      <li><a href="#">{$icon_folder}stark</a></li>
    </ul>
  </li>
    <li><a href="#">{$icon_script}index.php</a></li>
    <li><a href="#">{$icon_script}INSTALL.txt</a></li>
    <li><a href="#">{$icon_script}README.txt</a></li>
</ul>
HTML;

  return $form;
}

/**
 * Provide a progressbar widget for the Style guide page.
 *
 * FormAPI form builder callback.
 */
function styleguide_jquery_ui_demo_progressbar($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $form['#attributes']['class'][] = 'styleguide-jquery-ui-progressbar-wrapper';

  $form['controls'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'controls',
      ),
    ),
  );

  $form['controls']['type'] = array(
    '#type' => 'radios',
    '#required' => FALSE,
    '#title' => t('Type'),
    '#title_display' => 'none',
    '#default_value' => 'normal',
    '#options' => array(
      'normal' => t('Normal'),
      'indeterminate' => t('Indeterminate'),
    ),
    '#attributes' => array(
      'class' => array('styleguide-jquery-ui-buttonset'),
    ),
  );

  $form['controls']['slider'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'normal'),
      ),
    ),
  );

  $form['controls']['slider']['bar'] = array(
    '#markup' => '<div class="styleguide-jquery-ui-slider styleguide-jquery-ui-slider-hsi"></div>',
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.slider'),
      ),
    ),
  );

  $form['progressbar'] = array(
    '#markup' => '<div class="styleguide-jquery-ui-progressbar"><div class="progress-label">42</div></div>',
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.progressbar'),
      ),
    ),
  );

  return $form;
}

/**
 * Provide a slider widget for the Style guide page.
 *
 * FormAPI form builder callback.
 */
function styleguide_jquery_ui_demo_slider($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $prefix = 'styleguide-jquery-ui-slider';

  $path = drupal_get_path('module', 'styleguide_jquery_ui');
  foreach (array('h', 'v') as $direction) {
    $form['content'][$direction] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'styleguide-jquery-ui-slider-container-' . $direction,
        ),
      ),
      '#attached' => array(
        'css' => array(
          "$path/css/styleguide_jquery_ui.css",
        ),
        'js' => array(
          "$path/js/styleguide_jquery_ui.js",
        ),
        'library' => array(
          array('system', 'ui.slider'),
        ),
      ),
    );

    $form['content'][$direction]['sliders']['#markup'] = <<< HTML
<div class="$prefix $prefix-{$direction}s"></div>
<div class="$prefix $prefix-{$direction}si"></div>
<div class="$prefix $prefix-{$direction}sa"></div>
<div class="$prefix $prefix-{$direction}r"></div>
HTML;
  }

  return $form;
}

/**
 * @todo Vertical tabs left and right. Tabs at bottom.
 *
 * @return array
 *   Render array.
 */
function styleguide_jquery_ui_demo_tabs() {
  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $element = array(
    '#attached' => array(
      'js' => array(
        "$path/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.tabs'),
      ),
    ),
    '#prefix' => '<h3>' . t('Tabs') . '</h3><div class="styleguide-jquery-ui-tabs">',
    '#suffix' => '</div>',
    '#markup' => '',
  );

  $tabs = array();
  for ($i = 0; $i < 4; $i++) {
    $id = "jquery-ui-tabs-tab-$i";
    $nav = sprintf('<a href="#%s">%s</a>', $id, styleguide_word(rand(1, 3)));
    $tabs[$nav] = sprintf('<div id="%s">%s</div>',$id, styleguide_paragraph(rand(1, 3)));
  }
  $element['#markup'] .= '<ul><li>' . implode('</li><li>', array_keys($tabs)) . '</li></ul>';
  $element['#markup'] .= implode('', $tabs);

  return $element;
}

/**
 * Generate the jQuery UI icon list.
 */
function _styleguide_jquery_ui_demo_icon($form, &$form_state) {
  form_load_include($form_state, 'module', 'styleguide_jquery_ui', 'styleguide_jquery_ui.styleguide.inc');

  $path = drupal_get_path('module', 'styleguide_jquery_ui');

  $form['#attributes']['class'][] = 'styleguide-jquery-ui-icons-wrapper';

  $form['state_selector'] = array(
    '#type' => 'radios',
    '#required' => FALSE,
    '#title' => t('States'),
    '#options' => array('all' => t('All')) + _styleguide_jquery_ui_state_options(),
    '#default_value' => 'default',
    '#attributes' => array(
      'class' => array('styleguide-jquery-ui-buttonset'),
    ),
    '#attached' => array(
      'css' => array(
        "$path/css/styleguide_jquery_ui.css",
        "$path/css/styleguide_jquery_ui.recommendations.css",
      ),
      'js' => array(
        "{$path}/js/styleguide_jquery_ui.js",
      ),
      'library' => array(
        array('system', 'ui.button'),
      ),
    ),
  );
  _styleguide_jquery_ui_attach_recommendations($form['state_selector']);

  $form['state'] = array(
    '#prefix' => '<div class="styleguide-jquery-ui-icon-lists">',
    '#suffix' => '</div>',
  );

  foreach (_styleguide_jquery_ui_state() as $state_name => $state) {
    $form['state'][$state_name] = array(
      '#prefix' => sprintf('<div class="styleguide-jquery-ui-icon-list styleguide-jquery-ui-icon-list-state-%s">', $state_name),
      '#suffix' => '</div>',
    );

    $form['state'][$state_name]['title'] = array(
      '#markup' => '<h4>' . $state['label'] . '</h4>',
    );

    $form['state'][$state_name]['icons'] = array(
      '#type' => 'ul',
      '#theme' => 'item_list',
      '#items' => array(),
      '#attached' => array(
        'css' => array("$path/css/styleguide_jquery_ui.css"),
        'library' => array(
          array('system', 'ui'),
          array('system', 'ui.widget'),
        ),
      ),
      '#attributes' => array(
        'class' => array(
          'ui-widget',
          'ui-helper-clearfix',
          'icon-list',
        ),
      ),
    );

    foreach (_styleguide_jquery_ui_icons() as $icon) {
      $form['state'][$state_name]['icons']['#items'][$icon] = array(
        'class' => array(
          $state['class'],
          'ui-corner-all',
        ),
        'data' => sprintf(
          '<span title="%s" class="ui-icon ui-icon-%s"></span>',
          $icon,
          $icon
        )
      );
    }
  }

  return $form;
}
