/**
 * @file
 * Attach jQuery widgets on the StyleGuide page.
 */

(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.styleGuideJQueryUIAccordion = {
    attach: function (context, settings) {
      Drupal.styleGuideJQueryUI.attachPlugin(context, settings, 'accordion');
    }
  };

  Drupal.behaviors.styleGuideJQueryUIAutocomplete = {
    attach: function (context, settings) {
      Drupal.styleGuideJQueryUI.attachPlugin(context, settings, 'autocomplete', {
        source: Drupal.styleGuideJQueryUI.autocompleteSource
      });
    }
  };

  Drupal.behaviors.styleGuideJQueryUIButtonSet = {
    attach: function (context, settings) {
      Drupal.styleGuideJQueryUI.attachPlugin(context, settings, 'buttonset');
    }
  };

  Drupal.behaviors.styleGuideJQueryUIButton = {
    attach: function (context, settings) {
      if (!$.fn.button) {
        return;
      }

      var
        selectorPrefix = '.styleguide-jquery-ui-button',
        processedClass = 'styleguide-jquery-ui-button-processed';
      $(selectorPrefix + '-p:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: false,
          icons: {
            primary: 'ui-icon-heart'
          }
        });

      $(selectorPrefix + '-pt:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: true,
          icons: {
            primary: 'ui-icon-heart'
          }
        });

      $(selectorPrefix + '-pt:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: true,
          icons: {
            primary: 'ui-icon-heart'
          }
        });

      $(selectorPrefix + '-ts:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: true,
          icons: {
            secondary: 'ui-icon-star'
          }
        });

      $(selectorPrefix + '-pts:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: true,
          icons: {
            primary: 'ui-icon-heart',
            secondary: 'ui-icon-star'
          }
        });

      $(selectorPrefix + '-ps:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: false,
          icons: {
            primary: 'ui-icon-heart',
            secondary: 'ui-icon-star'
          }
        });

      $(selectorPrefix + '-s:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: false,
          icons: {
            secondary: 'ui-icon-star'
          }
        });

      $(selectorPrefix + '-t:not(.' + processedClass + ')', context)
        .addClass(processedClass)
        .button({
          text: true
        });
    }
  };

  Drupal.behaviors.styleGuideJQueryUIDatepicker = {
    attach: function (context, settings) {
      Drupal.styleGuideJQueryUI.attachPlugin(context, settings, 'datepicker');
    }
  };

  Drupal.behaviors.styleGuideJQueryUIDialog = {
    attach: function (context, settings) {
      if (!$.fn.dialog) {
        return;
      }

      var triggerClass = Drupal.styleGuideJQueryUI.triggerClass('dialog-opener');
      $(triggerClass.selector, context)
        .addClass(triggerClass.processed)
        .click(Drupal.styleGuideJQueryUI.dialogOpenerOnClick);
    }
  };

  Drupal.behaviors.styleGuideJQueryUIMenu =   {
    attach: function (context, settings) {
      $('.styleguide-jquery-ui-menu-wrapper ul', context).menu();
    }
  };

  Drupal.behaviors.styleGuideJQueryUIProgressbar = {
    attach: function (context, settings) {
      Drupal.styleGuideJQueryUI.attachPlugin(context, settings, 'progressbar', {
        value: 42,
        animate: true
      });
    }
  };

  Drupal.behaviors.styleGuideJQueryUISlider = {
    attach: function (context, settings) {
      if (!$.fn.slider) {
        return;
      }

      var
        selectorPrefix = '.' + Drupal.styleGuideJQueryUI.triggerClassPrefix + '-slider-',
        processedClass = Drupal.styleGuideJQueryUI.triggerClassPrefix + '-slider-processed',
        variants = {
          hs: {
            value: 42
          },
          hsi: {
            range: 'min',
            value: 42
          },
          hsa: {
            range: 'max',
            value: 42
          },
          hr: {
            range: true,
            values: [ 42, 63 ]
          },
          vs: {
            orientation: 'vertical',
            value: 42
          },
          vsi: {
            orientation: 'vertical',
            range: 'min',
            value: 42
          },
          vsa: {
            orientation: 'vertical',
            range: 'max',
            value: 42
          },
          vr: {
            orientation: 'vertical',
            range: true,
            values: [ 42, 63 ]
          }
        },
        variant;

      for (variant in variants) {
        if (variants.hasOwnProperty(variant)) {
          $(selectorPrefix + variant + ':not(.' + processedClass + ')', context)
            .addClass(processedClass)
            .slider(variants[variant]);
        }
      }
    }
  };

  Drupal.behaviors.styleGuideJQueryUITabs = {
    attach: function (context, settings) {
      Drupal.styleGuideJQueryUI.attachPlugin(context, settings, 'tabs');
    }
  };

  Drupal.behaviors.styleGuideJQueryUIFakeButton = {
    attach: function (context, settings) {
      var triggerClass = Drupal.styleGuideJQueryUI.triggerClass('fake-button');
      $(triggerClass.selector, context)
        .addClass(triggerClass.processed)
        .click(Drupal.styleGuideJQueryUI.fakeButtonOnClick);
    }
  };

  Drupal.behaviors.styleGuideJQueryUIDatepickerControls = {
    attach: function (context, settings) {
      var tc = Drupal.styleGuideJQueryUI.triggerClass('datepicker-controls');

      $(':input:not(.' + tc.processed + ')', context)
        .addClass(tc.processed)
        .change(Drupal.styleGuideJQueryUI.datepickerControlsOnChange);
    }
  };

  Drupal.behaviors.styleGuideJQueryUIProgressbarControls = {
    attach: function (context, settings) {
      var
        tcWrapper = Drupal.styleGuideJQueryUI.triggerClass('progressbar-wrapper'),
        tcSlider = Drupal.styleGuideJQueryUI.triggerClass('slider'),
        tc = Drupal.styleGuideJQueryUI.triggerClass('progressbar-controls');

      $(tcWrapper.base + ' ' + tcSlider.base + ':not(.' + tc.processed + ')', context)
        .addClass(tc.processed)
        .slider('option', 'change', Drupal.styleGuideJQueryUI.progressbarControlOnChange);

      $(tcWrapper.base + ' :input[name="type"]:not(.' + tc.processed + ')', context)
        .addClass(tc.processed)
        .change(Drupal.styleGuideJQueryUI.progressbarControlOnChange);
    }
  };

  Drupal.behaviors.styleGuideJQueryUIIconControls = {
    attach: function (context, settings) {
      var
        wrapperClass = 'styleguide-jquery-ui-icons-wrapper',
        processedClass = 'styleguide-jquery-ui-icon-list-selector-processed',
        groupName = 'state_selector';

      $('.' + wrapperClass + ' :input[name$="' + groupName + '"]:not(.' + processedClass + ')')
        .addClass(processedClass)
        .change(Drupal.styleGuideJQueryUI.IconControlsOnChange);

      $('.' + wrapperClass + ' :input[name$="' + groupName + '"]:checked').each(function () {
        Drupal.styleGuideJQueryUI.IconUpdate(
          $(this).parents('.' + wrapperClass),
          $(this).val()
        );
      });
    }
  };

  Drupal.styleGuideJQueryUI = Drupal.styleGuideJQueryUI || {};

  /**
   * jQuery UI icons.
   *
   * @type {string[]}
   */
  Drupal.styleGuideJQueryUI.autocompleteSource = [
    'blank',
    'carat-1-n',
    'carat-1-ne',
    'carat-1-e',
    'carat-1-se',
    'carat-1-s',
    'carat-1-sw',
    'carat-1-w',
    'carat-1-nw',
    'carat-2-n-s',
    'carat-2-e-w',
    'triangle-1-n',
    'triangle-1-ne',
    'triangle-1-e',
    'triangle-1-se',
    'triangle-1-s',
    'triangle-1-sw',
    'triangle-1-w',
    'triangle-1-nw',
    'triangle-2-n-s',
    'triangle-2-e-w',
    'arrow-1-n',
    'arrow-1-ne',
    'arrow-1-e',
    'arrow-1-se',
    'arrow-1-s',
    'arrow-1-sw',
    'arrow-1-w',
    'arrow-1-nw',
    'arrow-2-n-s',
    'arrow-2-ne-sw',
    'arrow-2-e-w',
    'arrow-2-se-nw',
    'arrowstop-1-n',
    'arrowstop-1-e',
    'arrowstop-1-s',
    'arrowstop-1-w',
    'arrowthick-1-n',
    'arrowthick-1-ne',
    'arrowthick-1-e',
    'arrowthick-1-se',
    'arrowthick-1-s',
    'arrowthick-1-sw',
    'arrowthick-1-w',
    'arrowthick-1-nw',
    'arrowthick-2-n-s',
    'arrowthick-2-ne-sw',
    'arrowthick-2-e-w',
    'arrowthick-2-se-nw',
    'arrowthickstop-1-n',
    'arrowthickstop-1-e',
    'arrowthickstop-1-s',
    'arrowthickstop-1-w',
    'arrowreturnthick-1-w',
    'arrowreturnthick-1-n',
    'arrowreturnthick-1-e',
    'arrowreturnthick-1-s',
    'arrowreturn-1-w',
    'arrowreturn-1-n',
    'arrowreturn-1-e',
    'arrowreturn-1-s',
    'arrowrefresh-1-w',
    'arrowrefresh-1-n',
    'arrowrefresh-1-e',
    'arrowrefresh-1-s',
    'arrow-4',
    'arrow-4-diag',
    'extlink',
    'newwin',
    'refresh',
    'shuffle',
    'transfer-e-w',
    'transferthick-e-w',
    'folder-collapsed',
    'folder-open',
    'document',
    'document-b',
    'note',
    'mail-closed',
    'mail-open',
    'suitcase',
    'comment',
    'person',
    'print',
    'trash',
    'locked',
    'unlocked',
    'bookmark',
    'tag',
    'home',
    'flag',
    'calculator',
    'cart',
    'pencil',
    'clock',
    'disk',
    'calendar',
    'zoomin',
    'zoomout',
    'search',
    'wrench',
    'gear',
    'heart',
    'star',
    'link',
    'cancel',
    'plus',
    'plusthick',
    'minus',
    'minusthick',
    'close',
    'closethick',
    'key',
    'lightbulb',
    'scissors',
    'clipboard',
    'copy',
    'contact',
    'image',
    'video',
    'script',
    'alert',
    'info',
    'notice',
    'help',
    'check',
    'bullet',
    'radio-off',
    'radio-on',
    'pin-w',
    'pin-s',
    'play',
    'pause',
    'seek-next',
    'seek-prev',
    'seek-end',
    'seek-first',
    'stop',
    'eject',
    'volume-off',
    'volume-on',
    'power',
    'signal-diag',
    'signal',
    'battery-0',
    'battery-1',
    'battery-2',
    'battery-3',
    'circle-plus',
    'circle-minus',
    'circle-close',
    'circle-triangle-e',
    'circle-triangle-s',
    'circle-triangle-w',
    'circle-triangle-n',
    'circle-arrow-e',
    'circle-arrow-s',
    'circle-arrow-w',
    'circle-arrow-n',
    'circle-zoomin',
    'circle-zoomout',
    'circle-check',
    'circlesmall-plus',
    'circlesmall-minus',
    'circlesmall-close',
    'squaresmall-plus',
    'squaresmall-minus',
    'squaresmall-close',
    'grip-dotted-vertical',
    'grip-dotted-horizontal',
    'grip-solid-vertical',
    'grip-solid-horizontal',
    'gripsmall-diagonal-se',
    'grip-diagonal-se'
  ];

  Drupal.styleGuideJQueryUI.dialogDefaultOptions = {
    width: '80%',
    height: 'auto',
    position: 'center'
  };

  Drupal.styleGuideJQueryUI.dialogClassNoTitle = 'no-title';

  Drupal.styleGuideJQueryUI.dialogClassNoClose = 'no-close';

  Drupal.styleGuideJQueryUI.triggerClassPrefix = 'styleguide-jquery-ui';

  /**
   * @param {string} subject
   *
   * @return {{prefix: string, base: string, selector: string, processed: string}}
   */
  Drupal.styleGuideJQueryUI.triggerClass = function (subject) {
    var
      prefix = Drupal.styleGuideJQueryUI.triggerClassPrefix,
      base = prefix + '-' + subject;

    return {
      prefix: prefix,
      base: '.' + base,
      selector: '.' + base + ':not(.' + base + '-processed)',
      processed: base + '-processed'
    };
  };

  Drupal.styleGuideJQueryUI.attachPlugin = function (context, settings, plugin, options) {
    if (!$.fn[plugin]) {
      return;
    }

    var triggerClass = Drupal.styleGuideJQueryUI.triggerClass(plugin),
      $subject = $(triggerClass.selector, context, settings);

    $subject.addClass(triggerClass.processed);

    if (options) {
      $subject[plugin](options);
    } else {
      $subject[plugin]();
    }
  };

  Drupal.styleGuideJQueryUI.datepickerControlsOnChange = function () {
    var
      $wrapper = $(this).parents('.styleguide-jquery-ui-demo-datepicker'),
      options = Drupal.styleGuideJQueryUI.datepickerOptions($wrapper),
      tc = Drupal.styleGuideJQueryUI.triggerClass('datepicker');

    $(tc.base + ':input', $wrapper).datepicker('option', options);

    options.showOn = 'focus';
    $(tc.base + ':not(:input)', $wrapper).datepicker('option', options);
  };

  Drupal.styleGuideJQueryUI.datepickerOptions = function ($wrapper) {
    var
      options = {
        showOtherMonths: false,
        selectOtherMonths: false,
        showButtonPanel: false,
        changeMonth: false,
        changeYear: false,
        showWeek: false,
        showOn: 'focus',
        buttonImage: '/misc/ui/images/ui-icons_cd0a0a_256x240.png',
        buttonImageOnly: false,
        numberOfMonths: 1
      },
      boolProps = [
        'showOtherMonths',
        'selectOtherMonths',
        'showButtonPanel',
        'changeMonth',
        'changeYear',
        'showWeek',
        'buttonImageOnly',
        'showOn'
      ],
      tpl = ':input[name="properties[%s]"]',
      i,
      name;

    for (i = 0; i < boolProps.length; i++) {
      name = boolProps[i];
      options[name] = $(tpl.replace('%s', name), $wrapper).is(':checked');
    }

    options.showOn = options.showOn ? 'button' : 'focus';

    options.numberOfMonths = [
      parseInt($(':input[name="numberOfMonthsRows"]', $wrapper).val(), 10),
      parseInt($(':input[name="numberOfMonthsColumns"]', $wrapper).val(), 10)
    ];

    return options;
  };

  Drupal.styleGuideJQueryUI.dialogOpenerOnClick = function (event) {
    event.preventDefault();

    Drupal.styleGuideJQueryUI.dialogOpen($(this).parents('.styleguide-jquery-ui-dialog-wrapper'));
  };

  Drupal.styleGuideJQueryUI.dialogOptions = function ($dialogWrapper) {
    var
      selectorTpl = ':input[name="properties[%]"]',
      options = $.extend({}, Drupal.styleGuideJQueryUI.dialogDefaultOptions);

    options.modal = $(selectorTpl.replace('%', 'modal'), $dialogWrapper).is(':checked');

    if ($(selectorTpl.replace('%', 'buttons'), $dialogWrapper).is(':checked')) {
      options.buttons = [
        {
          text: Drupal.t('Save', {}, {}),
          click: function () { $(this).dialog('close'); }
        },
        {
          text: Drupal.t('Cancel', {}, {}),
          click: function () { $(this).dialog('close'); }
        }
      ];
    } else {
      options.buttons = [];
    }

    options.dialogClass = [];
    if ($(selectorTpl.replace('%', 'no_title'), $dialogWrapper).is(':checked')) {
      options.dialogClass.push(Drupal.styleGuideJQueryUI.dialogClassNoTitle);
    }

    if ($(selectorTpl.replace('%', 'no_close'), $dialogWrapper).is(':checked')) {
      options.dialogClass.push(Drupal.styleGuideJQueryUI.dialogClassNoClose);
    }

    options.dialogClass = options.dialogClass.join(' ');

    return options;
  };

  Drupal.styleGuideJQueryUI.dialogOpen = function ($dialogWrapper, options) {
    var $dialog = $('#' + $dialogWrapper.attr('data-dialog-id'));

    if (options === undefined) {
      options = Drupal.styleGuideJQueryUI.dialogOptions($dialogWrapper);
    }

    if ($dialog.is(':data(dialog)')) {
      $dialog.dialog('option', options);
    } else {
      $dialog.dialog(options);
    }

    $dialog.dialog('open');
  };

  Drupal.styleGuideJQueryUI.IconControlsOnChange = function () {
    var
      $wrapper = $(this).parents('.styleguide-jquery-ui-icons-wrapper'),
      state = $(':input[name="' + $(this).attr('name') + '"]:checked', $wrapper).val();

    Drupal.styleGuideJQueryUI.IconUpdate($wrapper, state);
  };

  Drupal.styleGuideJQueryUI.IconUpdate = function ($wrapper, state) {
    var
      className = 'styleguide-jquery-ui-icon-list',
      classNameState = className + '-state-' + state;

    $('.' + className + ':not(.' + classNameState + ')', $wrapper).hide();
    if (state === 'all') {
      $('.styleguide-jquery-ui-icon-list', $wrapper).show();
    } else {
      $('.' + classNameState, $wrapper).show();
    }
  };

  Drupal.styleGuideJQueryUI.progressbarControlOnChange = function () {
    var $wrapper = $(this).parents('.styleguide-jquery-ui-progressbar-wrapper');

    Drupal.styleGuideJQueryUI.progressbarUpdate($wrapper);
  };

  Drupal.styleGuideJQueryUI.progressbarUpdate = function ($wrapper) {
    var
      type = $(':input[name="type"]:checked', $wrapper).val(),
      $slider = $('.styleguide-jquery-ui-slider', $wrapper),
      value = (type === 'normal' ? $slider.slider('value') : false),
      $progressbar = $('.styleguide-jquery-ui-progressbar', $wrapper);

    $progressbar.progressbar('option', 'value', value);
    $('.progress-label', $progressbar).html(
      value === false ? Drupal.t('Initialize...', {}, {}) : value
    );
  };

  /**
   * Indicate the button was clicked but do not submit the form.
   */
  Drupal.styleGuideJQueryUI.fakeButtonOnClick = function (event) {
    event.preventDefault();

    var $wrapper = $(this).parents('.styleguide-jquery-ui-button-wrapper');

    $('.styleguide-jquery-ui-feedback', $wrapper).html(Drupal.t(
      '%button was clicked %time',
      {
        '%button': $(this).text(),
        '%time': (new Date()).toISOString()
      },
      {}
    ));
  };

})(jQuery, Drupal);
